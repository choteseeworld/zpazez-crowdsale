const ZpazeZToken = artifacts.require("../contracts/ZpazeZToken.sol");
const ZpazeZTokenCrowdsale = artifacts.require(
  "../contracts/ZpazeZTokenCrowdsale.sol"
);

const Web3 = require("web3");

function ether(n) {
  return new Web3.utils.BN(Web3.utils.toWei(n, "ether"));
}

module.exports = async function (deployer, network, accounts) {
  // const latestTime = new Date().getTime();
  const _openingTime = new Web3.utils.BN(
    Math.floor(new Date(2021, 10, 23, 13, 0, 0, 0).getTime() / 1000) // Now + 5 Minute
  );
  const _closingTime = new Web3.utils.BN(
    Math.floor(new Date(2021, 10, 24, 20, 0, 0, 0).getTime() / 1000) // Year, Month, Date, Hour, Minute, Second
  );
  const rate = new Web3.utils.BN(20000); // At 20,000 Token/ETH
  const wallet = accounts[0];
  const goal = ether("5");
  const cap = ether("2");

  let token, crowdsale;

  await deployer
    .then(function () {
      return ZpazeZToken.new({ from: wallet });
    })
    .then(function (instance) {
      token = instance;
      return ZpazeZTokenCrowdsale.new(
        rate,
        wallet,
        token.address,
        _openingTime,
        _closingTime,
        cap,
        goal
      );
    })
    .then(function (instance) {
      crowdsale = instance;
      token.addMinter(crowdsale.address);
      token.renounceMinter();
      console.log("account[0]: " + wallet);
      console.log("Token address: ", token.address);
      console.log("Crowdsale address: ", crowdsale.address);
      return true;
    });
};
