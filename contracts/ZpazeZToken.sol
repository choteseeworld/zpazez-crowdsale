// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";

contract ZpazeZToken is ERC20Mintable {
    string public constant name = "ZpazeZToken";
    string public constant symbol = "ZZZ";
    uint8 public constant decimals = 18;

}