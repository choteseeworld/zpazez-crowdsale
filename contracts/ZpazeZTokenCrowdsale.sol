// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.8.0;

import "@openzeppelin/contracts/crowdsale/Crowdsale.sol";
import "@openzeppelin/contracts/crowdsale/validation/TimedCrowdsale.sol";
import "@openzeppelin/contracts/crowdsale/distribution/RefundableCrowdsale.sol";
import "@openzeppelin/contracts/crowdsale/validation/CappedCrowdsale.sol";
import "@openzeppelin/contracts/crowdsale/emission/MintedCrowdsale.sol";
import "@openzeppelin/contracts/crowdsale/distribution/PostDeliveryCrowdsale.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";

contract ZpazeZTokenCrowdsale is Crowdsale, TimedCrowdsale, RefundableCrowdsale, MintedCrowdsale, CappedCrowdsale, PostDeliveryCrowdsale {
    constructor(
        uint256 _rate,
        address payable _wallet,
        ERC20Mintable _token,
        uint256 _openingTime,
        uint256 _closingTime,
        uint256 _cap, 
        uint256 _goal
    ) 
        Crowdsale(_rate, _wallet, _token)
        TimedCrowdsale(_openingTime, _closingTime)
        RefundableCrowdsale(_goal)
        MintedCrowdsale()
        CappedCrowdsale(_cap)
        PostDeliveryCrowdsale()
        public
    {
        
    }

}